/**
 * @desc Navigation scroll handler
 * This module will check the document's scroll position and toggle a scroll
 * class on the body of the page. The CSS for the navigation is designed to
 * change background color if the user has scrolled
 * @namespace Application.Navigation
 * @memberof Application
 * @param {object} document
 * @returns {null}
 */
(function(d) {
  'use strict';
  //Navigation background toggle
  d.addEventListener('scroll', function() {
    var body = d.getElementsByTagName('body')[0];
    var offset = body.scrollTop;
    if (offset > 20) {
      body.className = 'scroll';
    } else {
      body.className = '';
    }
  });
})(document);

/**
 * @desc Aside click handler
 * This module registers a click handler for the aside menu button, which will toggle the
 * menu when clicked
 * @namespace Application.Menu
 * @memberof Application
 * @param {document}
 * @returns {null}
 */
(function(d) {
  'use strict';
  //Private variables for this module
  var asideOpen = false;
  
  //Some local methods
  function closeAside(e) {
    e.preventDefault();
    e.stopPropagation();
    asideOpen = false;
    aside.className = '';
  }

  //Register event listener
  var aside = d.getElementById('aside-overlay');
  aside.addEventListener('click', closeAside);

  var closeBtn = aside.querySelector('[data-action="close"]');
  closeBtn.addEventListener('click', closeAside);

  var innerAside = aside.getElementsByTagName('aside')[0];
  innerAside.addEventListener('click', function(e) {
    e.stopPropagation();
    return false;
  });

  var button = d.getElementById('aside-launch');
  button.addEventListener('click', function(e) {
    e.preventDefault();
    asideOpen = !asideOpen;
    var classNames = (asideOpen ? 'show-aside' : '');
    aside.className = classNames;
  });
})(document);
