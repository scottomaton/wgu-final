(function(d) {
	'use strict';

	var elems = d.querySelectorAll('input[type="radio"]');
	for (var k = 0; k < elems.length; ++k) {
		var elem = elems.item(k);
		elem.addEventListener('click', function(e) {
			var score = calcCorrectAnswers();
			d.getElementById('number-correct').innerHTML = score;
		});
	}

	function calcCorrectAnswers() {
		return d.querySelectorAll('input[type="radio"][value="true"]:checked').length
	}

})(document);