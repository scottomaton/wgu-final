# WGU Final

This repo contains the code for my Web Dev Fundamentals final. Normally, this would have build instructions and whatnot, but this is all just simple front-end stuff. I guess I don't even need to have this file, but old habits and such...

# Author

Scott Peterson

# License

CC BY
